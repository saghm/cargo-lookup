use chrono::{DateTime, Utc};
use serde::{de::IgnoredAny, Deserialize};

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum Response {
    NotFound(Error),
    CrateResponse(CrateResponse),
}

#[derive(Debug, Deserialize)]
pub struct Error {
    errors: IgnoredAny,
}

#[derive(Debug, Deserialize)]
pub struct CrateResponse {
    #[serde(rename = "crate")]
    pub crate_info: Crate,
    pub versions: Vec<Version>,
}

#[derive(Debug, Deserialize)]
pub struct Crate {
    pub description: String,
    pub downloads: u64,
    pub links: Links,
    pub max_stable_version: String,
    pub name: String,
    pub repository: String,
    pub recent_downloads: u64,
}

#[derive(Debug, Deserialize)]
pub struct Links {
    pub owners: String,
}

#[derive(Debug, Deserialize)]
pub struct Version {
    pub num: String,
    pub created_at: DateTime<Utc>,
    pub yanked: bool,
}

#[derive(Debug, Deserialize)]
pub struct Owners {
    pub users: Vec<User>,
}

#[derive(Debug, Deserialize)]
pub struct User {
    pub name: Option<String>,
    pub login: Option<String>,
}
