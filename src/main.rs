#![allow(dead_code)]

mod crates;

use crate::crates::{Owners, Response, Version as CrateVersion};
use anyhow::{bail, Result};
use chrono::{DateTime, Datelike, Utc};
use clap::{AppSettings, Parser};
use reqwest::blocking::Client;
use semver::Version;
use std::str::FromStr;

const CRATES_IO_API_BASE_URL: &str = "https://crates.io";
const CRATE_LOOKUP_URL: &str = "https://crates.io/api/v1/crates";

#[derive(Debug, Parser)]
#[clap(name = "cargo", author, version, about, setting = AppSettings::ArgRequiredElseHelp)]
#[clap(bin_name = "cargo")]
enum Cargo {
    Lookup(Lookup),
}

#[derive(Debug, Parser)]
#[clap(author, version, about, setting = AppSettings::ArgRequiredElseHelp)]
struct Lookup {
    /// Name of the crate to look up.
    crate_name: String,

    /// Number of versions to show.
    #[clap(short, long, default_value = "5")]
    num_versions: NumVersions,
}

/// How many versions to show.
#[derive(Debug)]
enum NumVersions {
    Count(usize),
    All,
}

impl FromStr for NumVersions {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self> {
        if s == "all" {
            return Ok(NumVersions::All);
        }

        let u = s.parse().map(NumVersions::Count)?;
        Ok(u)
    }
}

fn owners(client: &Client, url_path: &str) -> Result<impl Iterator<Item = String>> {
    let owners: Owners = client
        .get(format!("{CRATES_IO_API_BASE_URL}{url_path}"))
        .send()?
        .json()?;

    Ok(owners
        .users
        .into_iter()
        .filter_map(|user| user.name.or(user.login)))
}

fn get_stable_versions(
    versions: impl IntoIterator<Item = CrateVersion>,
) -> impl Iterator<Item = (String, DateTime<Utc>)> {
    versions.into_iter().filter_map(|v| {
        (!v.yanked && Version::parse(&v.num).ok()?.pre.is_empty()).then(|| (v.num, v.created_at))
    })
}

fn format_date(date: DateTime<Utc>) -> String {
    format!("{}/{}/{}", date.month(), date.day(), date.year())
}

fn parse() -> Lookup {
    let mut args = std::env::args().peekable();
    let first_arg = args
        .peek()
        .expect("this binary should not ever be invoked without at least arg0");

    if first_arg == "cargo-lookup" {
        Lookup::parse_from(args)
    } else {
        let Cargo::Lookup(lookup) = Cargo::parse_from(args);
        lookup
    }
}

fn main() -> Result<()> {
    let args = parse();

    let client = Client::builder()
        .user_agent(concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION")))
        .https_only(true)
        .build()?;

    let response = client
        .get(format!("{CRATE_LOOKUP_URL}/{}", args.crate_name))
        .send()?
        .json()?;
    let crate_response = match response {
        Response::NotFound(..) => {
            bail!("crate not found");
        }
        Response::CrateResponse(response) => response,
    };

    let owners: Vec<_> = owners(&client, &crate_response.crate_info.links.owners)?.collect();

    let title = format!(
        "{} - {}",
        crate_response.crate_info.name, crate_response.crate_info.max_stable_version
    );
    println!("{title}");

    for _ in 0..title.chars().count() {
        print!("-");
    }

    println!("\n{}\n", crate_response.crate_info.description.trim());

    println!("Owners:               {}", owners.join(", "));
    println!("Repository:           {}", crate_response.crate_info.repository);
    println!("Total downloads:      {}", crate_response.crate_info.downloads);
    println!("Recent downloads:     {}", crate_response.crate_info.recent_downloads);

    if let NumVersions::Count(0) = args.num_versions {
        return Ok(());
    }

    println!("\nVersions:");

    let num_versions = crate_response.versions.len();
    let max_versions = match args.num_versions {
        NumVersions::Count(n) => n,
        NumVersions::All => num_versions,
    };

    for (num, date) in get_stable_versions(crate_response.versions).take(max_versions) {
        println!("  {num:<11}         {}", format_date(date));
    }

    if num_versions > max_versions {
        eprintln!("  ... and {} more versions", num_versions - max_versions);
        eprintln!("\n  (run again specifying `--num-versions` to see more)")
    }

    Ok(())
}
